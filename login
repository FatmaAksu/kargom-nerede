/* Kargo takip uygulamasi icin web sitesi yapimi
Oncelikle login sayfasi duzenlendi.  - login.aspx -
Mssql ile baglanti kuruldu.
Giriste hem pnr koduna göre;hem de kullanici girisi yapılarak önceki siparislerin listesi gormek üzere 2 giris tasarlandi.
*/
using System;
using System.Data.SqlClient;

namespace KargomNerede
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CodeLabel.Text = "Gönderi kodunuzu giriniz.";
        }

        SqlConnection baglanti = new SqlConnection(" Data Source=LAPTOP-KHST3R1G\\MSSQLSERVER01;Initial Catalog=yazilim;Integrated Security=True");
        
        protected void loginButton_Click(object sender, EventArgs e)
        {
            String username = userNameTextBox.Text;
            String password = passwordTextBox.Text;
            
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select Kisi_id from Kisi where k_adi='" + username + "' and sifre='" + password + "'", baglanti);
            SqlDataReader dr = komut.ExecuteReader();

            if (dr.Read())        //Kullanici sistemde kayıtlı olup, bilgileri dogru ise devam eder.
            {
                dr.Close();
                SqlCommand komut2 = new SqlCommand("Select kullanici_tipi from Kisi  ", baglanti);
                SqlDataReader dr2 = komut2.ExecuteReader();
                
                //kullanici tipinin true olmasi durumunda musteri olarak algilayip, urunlerini gosterir.
                //kullanici tipinin false olmasi durumunda sirket elemani olarak algilayip, paket ekleme sayfasina yonlendirir.
                
                if (dr2.Read())  
                {
                    bool Kisi_tipi = Convert.ToBoolean(dr2.Read());
                    if (Kisi_tipi==false) {

                        KullaniciBilgileri girisKullanicisi = new KullaniciBilgileri();
                        girisKullanicisi.ad = username;
                        Session["girisKullanicisi"] = girisKullanicisi;
                        Response.Redirect("packageAdd.aspx");
                    }

                    else
                    {
                        KullaniciBilgileri girisKullanicisi = new KullaniciBilgileri();
                        int Kisi_id = Int32.Parse(dr["Kisi_id"].ToString());
                        girisKullanicisi.k_id = Kisi_id;
                        Session["girisKullanicisi"] = girisKullanicisi.k_id;
                        LogInfoLabel.Text = "";
                        Response.Redirect("list.aspx");
                    }
                }
                
                
            }
            else        //Kullanici bilgilerinin veri tabanindaki ile uyusmamasi halinde
            {
                LogInfoLabel.Text = "Kullanıcı adı veya şifreniz yanlış!";
                userNameTextBox.Text = "";
                passwordTextBox.Text = "";
            }
            baglanti.Close();
        }  
        
        protected void codeButton_Click(object sender, EventArgs e)   //pnr kodu girerek o anki kargoyu takip amaclı buton
        {
            int gonderi_No = Convert.ToInt32(pnrTextBox.Text);
            baglanti.Open();
            SqlCommand komut = new SqlCommand("Select * from Paket where Irsaliye_No='" + gonderi_No + "'", baglanti);
            SqlDataReader dr = komut.ExecuteReader();
            if (dr.Read())
            {
                KullaniciBilgileri girisKullanicisi = new KullaniciBilgileri();
                girisKullanicisi.gonderiNo = gonderi_No;
                Session["gonderiNo"] = girisKullanicisi.gonderiNo;
                Response.Redirect("orderList.aspx");
            }
            else
                CodeLabel.Text = "Böyle bir kargo takip numarası yoktur!";
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void recordButton_Click(object sender, EventArgs e)
        {

            Response.Redirect("record.aspx");
        }
    }
}